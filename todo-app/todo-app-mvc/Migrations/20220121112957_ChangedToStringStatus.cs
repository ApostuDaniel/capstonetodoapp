﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace todo_app_mvc.Migrations
{
    public partial class ChangedToStringStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Completed",
                table: "ToDoEntries");

            migrationBuilder.DropColumn(
                name: "InProgress",
                table: "ToDoEntries");

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "ToDoEntries",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "ToDoEntries");

            migrationBuilder.AddColumn<bool>(
                name: "Completed",
                table: "ToDoEntries",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "InProgress",
                table: "ToDoEntries",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
