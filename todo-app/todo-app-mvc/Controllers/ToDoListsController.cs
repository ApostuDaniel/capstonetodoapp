﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using todo_app_lib.Data;
using todo_app_lib.Models;
using todo_app_lib;

namespace todo_app_mvc.Controllers
{
    public class ToDoListsController : Controller
    {
        private readonly ToDoAppContext _context;

        public ToDoListsController(ToDoAppContext context)
        {
            _context = context;
        }

        // GET: ToDoLists
        public async Task<IActionResult> Index()
        {
            return View(await _context.ToDoLists.Include(list => list.ToDoEntries).ToListAsync());
        }

        // GET: ToDoLists/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoList = DbInteraction.ObtainList((int)id, _context);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        public IActionResult CopyList(int id)
        {
            try
            {
                var toDoList = DbInteraction.ObtainList(id, _context);
                DbInteraction.CreateToDoList(toDoList.Name, toDoList.ToDoEntries, _context);
            }
            catch (Exception e)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Index));
        }

        // GET: ToDoLists/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ToDoLists/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] ToDoList toDoList)
        {
            if (ModelState.IsValid)
            {
                _context.Add(toDoList);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        public IActionResult AddEntry(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddEntry(int id, [Bind("Title,Description,DueDate,Status")] ToDoEntry entry)
        {
            if (ModelState.IsValid)
            {
                DbInteraction.AssignEntry(id, entry, _context);
                return RedirectToAction(nameof(Edit), new {id = id});
            }
            return View(_context.ToDoLists
                .FirstOrDefaultAsync(m => m.Id == id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteEntry(int id, int listId)
        {
            try
            {
                DbInteraction.RemoveEntry(id, _context);
            }
            catch (Exception e)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Edit), new { id = listId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult StartTask(int id, int listId)
        {
            try
            {
                DbInteraction.StartTask(id, _context);
            }
            catch (Exception e)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Details), new {id = listId});
        }

        public IActionResult CompleteTask(int id, int listId)
        {
            try
            {
                DbInteraction.CompleteTask(id, _context);
            }
            catch (Exception e)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Details), new { id = listId });
        }

        public IActionResult EditEntry(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ToDoEntry entry;
            try
            {
                entry = DbInteraction.ObtainEntry(_context, (int)id);
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(entry);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditEntry(int id, [Bind("Id,Title,Description,DueDate,Status")] ToDoEntry entry)
        {
            if (id != entry.Id)
            {
                throw new ArgumentException("Form error, id changed");
            }

            int listId;
            if (ModelState.IsValid)
            {
                try
                {
                    listId = DbInteraction.ModifyEntry(id, entry, _context);
                }
                catch (Exception)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Edit), new { id = listId});
            }
            return View(entry);
        }

        // GET: ToDoLists/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ToDoList toDoList;
            try
            {
                toDoList = DbInteraction.ObtainList((int)id, _context);
            }
            catch (Exception)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] ToDoList toDoList)
        {
            if (id != toDoList.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(toDoList);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ToDoListExists(toDoList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(toDoList);
        }

        // GET: ToDoLists/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var toDoList = await _context.ToDoLists
                .FirstOrDefaultAsync(m => m.Id == id);
            if (toDoList == null)
            {
                return NotFound();
            }

            return View(toDoList);
        }

        // POST: ToDoLists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                DbInteraction.RemoveToDoList(id, _context);
            }
            catch(Exception e)
            {
                return NotFound();
            }     
            
            return RedirectToAction(nameof(Index));
        }

        private bool ToDoListExists(int id)
        {
            return _context.ToDoLists.Any(e => e.Id == id);
        }
    }
}
