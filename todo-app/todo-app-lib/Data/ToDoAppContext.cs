﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using todo_app_lib.Models;
using todo_app_lib;

namespace todo_app_lib.Data
{
    public class ToDoAppContext : DbContext
    {
        public ToDoAppContext (DbContextOptions<ToDoAppContext> options)
            : base(options)
        {
            //Helpers.RecreateCleanDatabase(this);
            //Helpers.PopulateDatabase(this);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.
            Entity<ToDoList>().
            HasMany(e => e.ToDoEntries).
            WithOne(e => e.ToDoList).OnDelete(DeleteBehavior.ClientCascade);
        }

        public DbSet<ToDoList> ToDoLists { get; set; }
        public DbSet<ToDoEntry> ToDoEntries { get; set; }


    }
}
