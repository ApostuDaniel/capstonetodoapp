﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using todo_app_lib.Models;
using todo_app_lib.Data;

namespace todo_app_lib
{
    public static class DbInteraction
    {
        public static void RemoveToDoList(string listName, ToDoAppContext db)
        {

            var list = ObtainList(listName, db);

            db.ToDoLists.Remove(list);

            db.SaveChanges();
        }

        public static void RemoveToDoList(int listId, ToDoAppContext db)
        {

            var list = ObtainList(listId, db);

            db.ToDoLists.Remove(list);

            db.SaveChanges();
        }



        public static void CreateToDoList(string name, IEnumerable<ToDoEntry> list, ToDoAppContext db)
        {
            var newList = new ToDoList { Name = name };
            db.ToDoLists.Add(newList);

            foreach (var entry in list)
            {
                var newEntry = new ToDoEntry
                {
                    Title = entry.Title,
                    Description = entry.Description,
                    Status = entry.Status,
                    DueDate = entry.DueDate
                };
                newList.ToDoEntries.Add(newEntry);
            }

            db.SaveChanges();
        }

        public static void AssignEntry(string listName, ToDoEntry entry, ToDoAppContext db)
        {
            if (entry == null)
            {
                throw new ArgumentNullException(nameof(entry), "Empty list entry");
            }

            var list = ObtainList(listName, db);

            list.ToDoEntries.Add(entry);

            db.SaveChanges();
        }

        public static void AssignEntry(int listId, ToDoEntry entry, ToDoAppContext db)
        {
            if (entry == null)
            {
                throw new ArgumentNullException(nameof(entry), "Empty list entry");
            }

            var list = ObtainList(listId, db);

            list.ToDoEntries.Add(entry);

            db.SaveChanges();
        }

        public static int ModifyEntry(string listName, string entryTitle, ToDoEntry newEntry, ToDoAppContext db)
        {

            var list = ObtainList(listName, db);
            var entry = ObtainEntry(list, entryTitle);

            if (!string.IsNullOrEmpty(newEntry.Title))
            {
                entry.Title = newEntry.Title;
            }

            if (newEntry.Description != null) entry.Description = newEntry.Description;
            if (newEntry.DueDate != DateTime.MinValue) entry.DueDate = newEntry.DueDate;
            entry.Status = newEntry.Status;

            db.SaveChanges();

            return entry.ToDoListId;
        }

        public static int ModifyEntry(int entryId, ToDoEntry newEntry, ToDoAppContext db)
        {
            var entry = ObtainEntry(db, entryId);

            if (!string.IsNullOrEmpty(newEntry.Title))
            {
                entry.Title = newEntry.Title;
            }

            if (newEntry.Description != null) entry.Description = newEntry.Description;
            if (newEntry.DueDate != DateTime.MinValue) entry.DueDate = newEntry.DueDate;
            entry.Status = newEntry.Status;
            

            db.SaveChanges();

            return entry.ToDoListId;
        }

        public static void RemoveEntry(string listname, string entryTitle, ToDoAppContext db)
        {

            var list = ObtainList(listname, db);
            var entry = ObtainEntry(list, entryTitle);

            db.ToDoEntries.Remove(entry);

            db.SaveChanges();
        }

        public static void RemoveEntry(int listId, string entryTitle, ToDoAppContext db)
        {
            var list = ObtainList(listId, db);
            var entry = ObtainEntry(list, entryTitle);

            db.ToDoEntries.Remove(entry);

            db.SaveChanges();
        }

        public static void RemoveEntry(int entryId, ToDoAppContext db)
        {
            var entry = ObtainEntry(db, entryId);

            db.ToDoEntries.Remove(entry);

            db.SaveChanges();
        }

        public static void CompleteTask(int entryId, ToDoAppContext db)
        {
            var entry = ObtainEntry(db, entryId);
            entry.Status = "Completed";
           
            db.SaveChanges();
        }

        public static void StartTask(int entryId, ToDoAppContext db)
        {
            var entry = ObtainEntry(db, entryId);

            entry.Status ="In Progress";

            db.SaveChanges();
        }



        public static ToDoList ObtainList(int listId, ToDoAppContext db)
        {
            if (db == null)
                throw new ArgumentNullException(nameof(db));

            var list = db.ToDoLists.Include(e => e.ToDoEntries).FirstOrDefault(e => e.Id == listId);
            if (list == null)
            {
                throw new ArgumentException(nameof(listId));
            }

            return list;
        }

        public static ToDoList ObtainList(string listName, ToDoAppContext db)
        {
            if (db == null)
                throw new ArgumentNullException(nameof(db));

            if (string.IsNullOrEmpty(listName))
            {
                throw new ArgumentNullException(nameof(listName), "List name can't be null");
            }

            var list = db.ToDoLists.Include(e => e.ToDoEntries).FirstOrDefault(e => e.Name == listName);
            if (list == null)
            {
                throw new ArgumentException(nameof(list));
            }

            return list;
        }

        public static ToDoEntry ObtainEntry(ToDoAppContext db, int entryId)
        {
            if (db == null)
                throw new ArgumentNullException(nameof(db));

            var entry = db.ToDoEntries.Find(new object[] { entryId });

            if (entry == null)
            {
                throw new ArgumentException(nameof(entry));
            }

            return entry;
        }
        public static ToDoEntry ObtainEntry(ToDoList list, string entryTitle)
        {
            if (string.IsNullOrEmpty(entryTitle))
            {
                throw new ArgumentNullException(nameof(entryTitle), "EntryTitle can't be null");
            }

            if (list == null)
                throw new ArgumentNullException(nameof(list));

            var entry = list.ToDoEntries.FirstOrDefault(e => e.Title == entryTitle);

            if (entry == null)
            {
                throw new ArgumentException(nameof(entry));
            }

            return entry;
        }

        private static void ModifyListValues(ToDoList list, string newListName, IEnumerable<ToDoEntry> entries)
        {
            if (!string.IsNullOrEmpty(newListName))
            {
                list.Name = newListName;
            }


            if (entries != null)
            {
                list.ToDoEntries.Clear();

                foreach (var entry in entries)
                {
                    list.ToDoEntries.Add(entry);
                }
            }
        }

        public static void ModifyList(int listId, string newListName, IEnumerable<ToDoEntry> entries, ToDoAppContext db)
        {
            var list = ObtainList(listId, db);

            ModifyListValues(list, newListName, entries);

            db.SaveChanges();
        }


        public static void ModifyList(string listname, string newListName, IEnumerable<ToDoEntry> entries, ToDoAppContext db)
        {
            var list = ObtainList(listname, db);

            ModifyListValues(list, newListName, entries);

            db.SaveChanges();
        }
    }
}
