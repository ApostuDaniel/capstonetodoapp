﻿using System;
using System.ComponentModel.DataAnnotations;

namespace todo_app_lib.Models
{
    public class ToDoEntry
    {
        public int Id { get; set; }
        public string Title { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        [Display(Name ="Due Date")]
        [DataType(DataType.Date)]
        public DateTime DueDate { get; set; }
        [Required]
        public string Status { get; set; } = "Not Started";
        public int ToDoListId { get; set; }
        public ToDoList ToDoList { get; set; }

        public override bool Equals(object obj) => this.Equals(obj as ToDoEntry);

        public bool Equals(ToDoEntry e)
        {
            if (e is null)
            {
                return false;
            }


            if (Object.ReferenceEquals(this, e))
            {
                return true;
            }


            if (this.GetType() != e.GetType())
            {
                return false;
            }


            if (this.Title != e.Title || this.Description != e.Description 
                || this.DueDate != e.DueDate || this.Status != e.Status) return false;

            return true;
        }

        public static bool operator ==(ToDoEntry lhs, ToDoEntry rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ToDoEntry lhs, ToDoEntry rhs) => !(lhs == rhs);
    }
}
