﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace todo_app_lib.Models
{
    public class ToDoList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Display(Name="Tasks")]
        public IList<ToDoEntry> ToDoEntries { get; } = new List<ToDoEntry>();

        public override bool Equals(object obj) => this.Equals(obj as ToDoList);

        public bool Equals(ToDoList l)
        {
            if (l is null)
            {
                return false;
            }

            
            if (Object.ReferenceEquals(this, l))
            {
                return true;
            }

           
            if (this.GetType() != l.GetType())
            {
                return false;
            }

           
            if (this.Name != l.Name) return false;

            var list1 = this.ToDoEntries.ToArray();
            var list2 = l.ToDoEntries.ToArray();
            if (list1.Length != list2.Length) return false;

            for (int i = 0; i < list1.Length; i++)
            {
                if (list1[i] != list2[i]) return false;
            }

            return true;  
        }

        public static bool operator ==(ToDoList lhs, ToDoList rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                {
                    return true;
                }

                return false;
            }
            return lhs.Equals(rhs);
        }

        public static bool operator !=(ToDoList lhs, ToDoList rhs) => !(lhs == rhs);
    }
}
