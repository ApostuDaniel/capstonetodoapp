﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using todo_app_lib.Data;
using todo_app_lib.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace todo_app_lib
{
    public static class Helpers
    {
        public static void RecreateCleanDatabase(ToDoAppContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }

        public static void PopulateDatabase(ToDoAppContext context)
        {
            if (context.ToDoLists.Any()) return;

            context.Add(
                new ToDoList
                {
                    Name = "Chores",
                    ToDoEntries =
                    {
                        new ToDoEntry
                        {
                            Title = "Wash the dishes",
                            Description = "The sink is full, you should probably wash the dishes",
                            DueDate = new DateTime(2021, 11, 30),
                            Status = "Completed"
                        },
                        new ToDoEntry
                        {
                            Title = "Clean the room",
                            Description = "Dust is starting to accumulate, you should have cleaned",
                            DueDate = DateTime.Now,
                            Status = "In Progress"
                        },
                    }
                });
            context.Add(
                 new ToDoList
                 {
                     Name = "Exams",
                     ToDoEntries =
                    {
                        new ToDoEntry
                        {
                            Title = "Study for LFAC",
                            Description = "The LFAC exam is composed of 2 tests from the first and second part of the semester",
                            DueDate = new DateTime(2022, 1, 26),
                            Status = "Not Started"
                        },
                        new ToDoEntry
                        {
                            Title = "Study of Databases exam",
                            Description = "Consists of a test consisting of the material from the second part of the semester",
                            DueDate = new DateTime(2022, 2, 2),
                            Status = "In Progress"
                        },
                    }
                 }
                );

            context.SaveChanges();
        }

        public static void Initialize(IServiceProvider serviceProvider)
        {

            using var context = new ToDoAppContext(
               serviceProvider.GetRequiredService<
                   DbContextOptions<ToDoAppContext>>());

            if (context.ToDoLists.Any()) return;

            PopulateDatabase(context);
        }
    }
}
