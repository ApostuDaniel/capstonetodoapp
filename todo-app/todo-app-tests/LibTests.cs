﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using todo_app_lib;
using todo_app_lib.Data;
using todo_app_lib.Models;

namespace todo_app_test
{
    [TestFixture]
    public class DBInteractionsTest
    {
        public ToDoAppContext db;
        [SetUp]
        public void SetUpDataBase()
        {
            DbContextOptionsBuilder<ToDoAppContext> options = new();
            db = new ToDoAppContext(options.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;Integrated Security=True;
                Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;
                ApplicationIntent=ReadWrite;MultiSubnetFailover=False;Database=ToDoTestDB;
                Trusted_Connection=True").Options);
        }

        [TearDown]
        public void DisposeContext()
        {
            db.Dispose();
        }


        [Test]
        public void ObtainListByIDNoContextThrowsNull()
        {
            Assert.Throws<ArgumentNullException>(() => DbInteraction.ObtainList(1, null));
        }

        [Test]
        public void ObtainListByNameNoContextThrowsNull()
        {
            Assert.Throws<ArgumentNullException>(() => DbInteraction.ObtainList("name", null));
        }

        [Test]
        public void ObtainListByIdBadIdThrows()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainList(-1, db));
            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainList(3, db));
        }

        [Test]
        public void ObtainListBynameBadNameThrows()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainList("Inexistent name", db));
        }

        [Test]
        public void ObtainEntryEmptyTitleThrowsNull()
        {
            Assert.Throws<ArgumentNullException>(() => DbInteraction.ObtainEntry(new ToDoList { Name = "Placehoder" }, null));
        }

        [Test]
        public void ObtainEntryNullListThrowsNull()
        {
            Assert.Throws<ArgumentNullException>(() => DbInteraction.ObtainEntry(null, "Placeholder name"));
        }

        [Test]
        public void ObtainEntryNullContextThrowsNull()
        {
            Assert.Throws<ArgumentNullException>(() => DbInteraction.ObtainEntry(null, 1));
        }

        [Test]
        public void ObtainEntryBadIdThrowsNull()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainEntry(db, 5));
            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainEntry(db, -1));
        }

        [Test]
        public void CreateToDoListSuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            var entries = new ToDoEntry[] { new ToDoEntry { Title = "TestEntry", Description = "testDescription", Status = "Not Started", DueDate = DateTime.Now } };
            DbInteraction.CreateToDoList("TestList", entries,db);

            var expected = new ToDoList { Name = "TestList" };
            foreach (var entry in entries)
            {
                expected.ToDoEntries.Add(entry);
            }

            ToDoList actual = DbInteraction.ObtainList("TestList", db);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveToDoListSuccess()
        {

            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            var entries = new ToDoEntry[] { new ToDoEntry { Title = "TestEntry", Description = "testDescription", Status = "Completed", DueDate = DateTime.Now } };
            DbInteraction.CreateToDoList("TestList", entries, db);
            DbInteraction.RemoveToDoList("Chores", db);

            var expected = new ToDoList { Name = "TestList" };
            foreach (var entry in entries)
            {
                expected.ToDoEntries.Add(entry);
            }

            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainList("Chores", db));
            ToDoList actual = DbInteraction.ObtainList("TestList", db);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AssignEntrySuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            var expected = new ToDoEntry { Title = "TestEntry", Description = "TestDescritpion", Status = "In Progress", DueDate = DateTime.Now };

            DbInteraction.AssignEntry("Chores", expected, db);

            var actual = DbInteraction.ObtainEntry(db, 5);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ModifyEntrySuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            var expected = new ToDoEntry { Title = "TestEntry", Description = "TestDescritpion", Status = "In progress", DueDate = DateTime.Now };

            DbInteraction.ModifyEntry(2, expected, db);

            var actual = DbInteraction.ObtainEntry(db, 2);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void RemoveEntrySuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);

            DbInteraction.RemoveEntry(2, db);

            Assert.Throws<ArgumentException>(() => DbInteraction.ObtainEntry(db, 2));
        }

        [Test]
        public void CompleteTaskSuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);
            DbInteraction.CompleteTask(2, db);
            var entry2 = DbInteraction.ObtainEntry(db, 2);

            Assert.IsTrue(entry2.Status == "Completed");
        }

        [Test]
        public void ModifyListSuccess()
        {
            Helpers.RecreateCleanDatabase(db);
            Helpers.PopulateDatabase(db);

            var expected = new ToDoList
            {
                Name = "New List Name"
            };

            expected.ToDoEntries.Add(new ToDoEntry { Title = "Test entry", Description = "Test Description", DueDate = DateTime.Now, Status = "Completed" });

            DbInteraction.ModifyList(1, expected.Name, expected.ToDoEntries.ToArray(), db);

            var actual = DbInteraction.ObtainList(1, db);

            Assert.AreEqual(expected, actual);
        }
    }
}